package com.domsd.jobmatcher.service.matcher;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.domsd.jobmatcher.exception.ErrorMessage;
import com.domsd.jobmatcher.model.Job;
import com.domsd.jobmatcher.model.Worker;
import com.domsd.jobmatcher.repository.JobRepository;
import com.domsd.jobmatcher.repository.WorkerRepository;
import com.domsd.jobmatcher.service.configuration.ConfigurationService;
import com.domsd.jobmatcher.service.matcher.criteria.sorting.SortingCriteriaToComparator;
import com.domsd.jobmatcher.testutils.TestUtils;

public class JobMatcherServiceUnitTest {

    @InjectMocks
    private JobMatcherServiceImpl jobMatcherService;

    @Mock
    private JobRepository jobRepository;

    @Mock
    private WorkerRepository workerRepository;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private JobMatchCheckerService matchChecker;

    @Mock
    private SortingCriteriaToComparator sortingCriteriaToComparator;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void matches_nullWorkerIdentifier_shouldThrowException() {

        TestUtils.callExpectingApplicationException(() -> {

            jobMatcherService.matches(null);

        }, ErrorMessage.UNEXPECTED_NULL_IDENTIFIER);
    }

    @Test
    public void matches_WorkerNotFound_shouldThrowException() {
        
        TestUtils.callExpectingApplicationException(() -> {
            
            long workerId = 10l;
            
            when(workerRepository.retrieveWorker(workerId)).thenReturn(null);
            
            jobMatcherService.matches(workerId);
            
            
        }, ErrorMessage.WORKER_NOT_FOUND);
    }

    @Test
    public void matches_WorkerFound_3Matches_LimitIsOne_ShouldReturnOneJob() {
        
            
            long workerId = 10l;
            Worker worker = new Worker();
            List<Job> jobs = Arrays.asList(new Job(), new Job(), new Job());
            
            when(workerRepository.retrieveWorker(workerId)).thenReturn(worker);
            when(jobRepository.retrieveAllJobs()).thenReturn(jobs);
            when(configurationService.maximumNumberOfJobMatches()).thenReturn(1);
            when(matchChecker.allCriteriaMatched(any(), Mockito.eq(worker), any())).thenReturn(true);
            when(sortingCriteriaToComparator.toComparator(any())).thenReturn(alwaysEqualComparator());
            
            List<Job> matches = jobMatcherService.matches(workerId);
            
            assertEquals(1,  matches.size());
    }
    
    @Test
    public void matches_WorkerFound_3Matches_LimitIsThree_ShouldReturnThreeJobs() {
        
            
            long workerId = 10l;
            Worker worker = new Worker();
            List<Job> jobs = Arrays.asList(new Job(), new Job(), new Job());
            
            when(workerRepository.retrieveWorker(workerId)).thenReturn(worker);
            when(jobRepository.retrieveAllJobs()).thenReturn(jobs);
            when(configurationService.maximumNumberOfJobMatches()).thenReturn(3);
            when(matchChecker.allCriteriaMatched(any(), Mockito.eq(worker), any())).thenReturn(true);
            when(sortingCriteriaToComparator.toComparator(any())).thenReturn(alwaysEqualComparator());
            
            List<Job> matches = jobMatcherService.matches(workerId);
            
            assertEquals(3,  matches.size());
    }
    

    private Comparator<Job> alwaysEqualComparator() {
        
        return new Comparator<Job>() {
            
            @Override
            public int compare(Job o1, Job o2) {
                return 0;
            }
        };
    }

    
}
