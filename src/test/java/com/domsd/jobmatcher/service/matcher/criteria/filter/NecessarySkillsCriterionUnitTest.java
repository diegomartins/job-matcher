package com.domsd.jobmatcher.service.matcher.criteria.filter;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.domsd.jobmatcher.model.Job;
import com.domsd.jobmatcher.model.Worker;
import com.domsd.jobmatcher.testutils.TestUtils;

public class NecessarySkillsCriterionUnitTest {
    
    
    private NecessarySkillsCriterion necessarySkillsCriterion;
    
    @Before
    public void setup() {
        necessarySkillsCriterion = new NecessarySkillsCriterion();
    }
    
    
    @Test
    public void matched_workerHasTheSkill_shouldReturnTrue() {
        
        String jobTitle = "A";
        Set<String> workerSkills = TestUtils.asSet("B", "A", "C");
        
        boolean matchedTheCriterion = buildAndRunBasicScenario(jobTitle, workerSkills);
        
        assertTrue(matchedTheCriterion);
    }
    
    @Test
    public void matched_workerDoesntHaveTheSkill_shouldReturnFalse() {
        
        String jobTitle = "D";
        Set<String> workerSkills = TestUtils.asSet("B", "A", "C");
        
        boolean matchedTheCriterion = buildAndRunBasicScenario(jobTitle, workerSkills);
        
        assertFalse(matchedTheCriterion);
    }
    
    @Test
    public void matched_workerSkillsAreNull_shouldReturnFalse() {
        
        String jobTitle = "D";
        Set<String> workerSkills = null;
        
        boolean matchedTheCriterion = buildAndRunBasicScenario(jobTitle, workerSkills);
        
        assertFalse(matchedTheCriterion);
    }
  
    
    private boolean buildAndRunBasicScenario(String jobTitle, Set<String> workerSkills) {
        
        // Configurations
        Job job = new Job();
        Worker worker = new Worker();
        
        job.setJobTitle(jobTitle);
        worker.setSkills(workerSkills);
        
        // Action
        boolean criterionMatched = necessarySkillsCriterion.matched(worker, job);
        
        
        return criterionMatched;
    }
    

}
