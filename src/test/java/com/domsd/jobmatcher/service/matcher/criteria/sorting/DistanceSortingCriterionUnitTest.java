package com.domsd.jobmatcher.service.matcher.criteria.sorting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.RETURNS_SELF;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Comparator;

import org.apache.commons.lang3.RandomUtils;
import org.junit.Before;
import org.junit.Test;

import com.domsd.jobmatcher.model.Job;
import com.domsd.jobmatcher.model.JobSearchAddress;
import com.domsd.jobmatcher.model.Location;
import com.domsd.jobmatcher.service.geo.AbstractDistanceCalculator;

public class DistanceSortingCriterionUnitTest {

    private static final Double LAT_LOCATION_1 = RandomUtils.nextDouble(0, 100);
    private static final Double LON_LOCATION_1 = RandomUtils.nextDouble(0, 100);
    private static final Double LAT_LOCATION_2 = RandomUtils.nextDouble(0, 100);
    private static final Double LON_LOCATION_2 = RandomUtils.nextDouble(0, 100);
    
    private static final Double LAT_JOB_SEARCH = RandomUtils.nextDouble(0, 100);
    private static final Double LON_JOB_SEARCH = RandomUtils.nextDouble(0, 100);
    
        
    private DistanceSortingCriterion distanceSortingCriterion;
    
    private AbstractDistanceCalculator distanceCalculator;
    
    private AbstractDistanceCalculator calculatorForLocation1;
    private AbstractDistanceCalculator calculatorForLocation2;
    
    private JobSearchAddress jobSearchAddress;
    
    @Before
    public void setup() {
    	jobSearchAddress = getJobSearchAddress(LAT_JOB_SEARCH, LON_JOB_SEARCH);
        
    	distanceCalculator = mock(AbstractDistanceCalculator.class, RETURNS_SELF);
        calculatorForLocation1 = mock(AbstractDistanceCalculator.class, RETURNS_SELF);
        calculatorForLocation2 = mock(AbstractDistanceCalculator.class, RETURNS_SELF);
        
        distanceSortingCriterion = new DistanceSortingCriterion(jobSearchAddress, distanceCalculator);
    }
    
    @Test
    public void toComparator_sameDistance_shouldReturn0() {
        
        Location location1 = getLocation(LAT_LOCATION_1, LON_LOCATION_1);
        Location location2 = getLocation(LAT_LOCATION_2, LON_LOCATION_2);
        
        int result = buildAndRunBasicScenario(location1, location2, 30, 30);
        
        shouldBeZero(result);
    }
    
    @Test
    public void toComparator_location1IsFarther_shouldReturnPositive() {
        
        Location location1 = getLocation(LAT_LOCATION_1, LON_LOCATION_1);
        Location location2 = getLocation(LAT_LOCATION_2, LON_LOCATION_2);
        
        int result = buildAndRunBasicScenario(location1, location2, 30, 20);
        
        shouldBePositive(result);
    }
    
    @Test
    public void toComparator_location1IsNearer_shouldReturnNegative() {
        
        Location location1 = getLocation(LAT_LOCATION_1, LON_LOCATION_1);
        Location location2 = getLocation(LAT_LOCATION_2, LON_LOCATION_2);
        
        int result = buildAndRunBasicScenario(location1, location2, 20, 30);
        
        shouldBeNegative(result);
    }
    
    
    @Test
    public void toComparator_location1LatitudeWasNotDefined_shouldReturnPositive() {
        
        Location location1 = getLocation(null, LON_LOCATION_1);
        Location location2 = getLocation(LAT_LOCATION_2, LON_LOCATION_2);
        
        int result = buildAndRunNullableLocationsScenario(location1, location2);
        
        shouldBePositive(result);
    }
    
    @Test
    public void toComparator_location1LongitudeWasNotDefined_shouldReturnPositive() {
        
        Location location1 = getLocation(LAT_LOCATION_1, null);
        Location location2 = getLocation(LAT_LOCATION_2, LON_LOCATION_2);
        
        int result = buildAndRunNullableLocationsScenario(location1, location2);
        
        shouldBePositive(result);
    }
    
    @Test
    public void toComparator_location1IsNull_shouldReturnPositive() {
        
        Location location1 = null;
        Location location2 = getLocation(LAT_LOCATION_2, LON_LOCATION_2);
        
        int result = buildAndRunNullableLocationsScenario(location1, location2);
        
        shouldBePositive(result);
    }
    
    @Test
    public void toComparator_location2LatitudeWasNotDefined_shouldReturnNegative() {
        
        Location location1 = getLocation(LAT_LOCATION_1, LON_LOCATION_1);
        Location location2 = getLocation(null, LON_LOCATION_2);
        
        int result = buildAndRunNullableLocationsScenario(location1, location2);
        
        shouldBeNegative(result);
    }
    
    @Test
    public void toComparator_location2LongitudeWasNotDefined_shouldReturnNegative() {
        
        Location location1 = getLocation(LAT_LOCATION_1, LON_LOCATION_1);
        Location location2 = getLocation(LAT_LOCATION_2, null);
        
        int result = buildAndRunNullableLocationsScenario(location1, location2);
        
        shouldBeNegative(result);
    }
    
    @Test
    public void toComparator_location2IsNull_shouldReturnNegative() {
        
        Location location1 = getLocation(LAT_LOCATION_1, LON_LOCATION_1);
        Location location2 = getLocation(null, LON_LOCATION_2);
        
        int result = buildAndRunNullableLocationsScenario(location1, location2);
        
        shouldBeNegative(result);
    }
    
    @Test
    public void toComparator_location1AndLocation2AreNull_shouldReturnZero() {
        
        Location location1 = null;
        Location location2 = null;
        
        int result = buildAndRunNullableLocationsScenario(location1, location2);
        
        shouldBeZero(result);
    }
    
    @Test
    public void toComparator_location1AndLocation2HaveCoordinatesNotProperlyDefined_shouldReturnZero() {
        
        Location location1 = getLocation(LAT_LOCATION_1, null);
        Location location2 = getLocation(null, LON_LOCATION_2);
        
        int result = buildAndRunNullableLocationsScenario(location1, location2);
        
        shouldBeZero(result);
    }
    
    @Test
    public void toComparator_jobSearchAddressIsNull_shouldReturnZero() {
        
        Location location1 = getLocation(LAT_LOCATION_1, LON_LOCATION_1);
        Location location2 = getLocation(LAT_LOCATION_2, LON_LOCATION_2);
        
        int result = buildAndRunNullableJobSearchAddressScenario(location1, location2, null);
        
        shouldBeZero(result);
    }
    
    
    public int buildAndRunBasicScenario(Location location1, Location location2, double distanceFromLocation1, double distanceFromLocation2) {
        
        Job job1 = new Job();
        job1.setLocation(location1);
        Job job2 = new Job();
        job2.setLocation(location2);
        
        when(distanceCalculator.from(LAT_LOCATION_1, LON_LOCATION_1)).thenReturn(calculatorForLocation1);
        when(distanceCalculator.from(LAT_LOCATION_2, LON_LOCATION_2)).thenReturn(calculatorForLocation2);
        
        when(calculatorForLocation1.calculate()).thenReturn(distanceFromLocation1);
        when(calculatorForLocation2.calculate()).thenReturn(distanceFromLocation2);
        
        Comparator<Job> comparator = distanceSortingCriterion.toComparator();
        int result = comparator.compare(job1, job2);
        
        return result;
    }
    
    public int buildAndRunNullableLocationsScenario(Location location1, Location location2) {
        
        Job job1 = new Job();
        job1.setLocation(location1);
        Job job2 = new Job();
        job2.setLocation(location2);
        
        Comparator<Job> comparator = distanceSortingCriterion.toComparator();
        int result = comparator.compare(job1, job2);
        
        return result;
    }
    
    public int buildAndRunNullableJobSearchAddressScenario(Location location1, Location location2, JobSearchAddress jobSearchAddress) {
        
        Job job1 = new Job();
        job1.setLocation(location1);
        Job job2 = new Job();
        job2.setLocation(location2);
        
        distanceSortingCriterion = new DistanceSortingCriterion(jobSearchAddress);
        
        Comparator<Job> comparator = distanceSortingCriterion.toComparator();
        int result = comparator.compare(job1, job2);
        
        return result;
    }


    private Location getLocation(Double latitude, Double longitude) {
        
        Location location = new Location();
        
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        
        return location;
    }
    
    private JobSearchAddress getJobSearchAddress(Double latitude, Double longitude) {
        
        JobSearchAddress jobSearchAddress = new JobSearchAddress();
        
        jobSearchAddress.setLatitude(latitude);
        jobSearchAddress.setLongitude(longitude);
        
        return jobSearchAddress;
    }
    
    
    private void shouldBeZero(int result) {
        assertEquals(0, result);
    }
    
    private void shouldBeNegative(int result) {
        assertTrue(result < 0);
    }
    
    private void shouldBePositive(int result) {
        assertTrue(result > 0);
    }
    

}
