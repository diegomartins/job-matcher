package com.domsd.jobmatcher.service.matcher.criteria.filter;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.domsd.jobmatcher.model.Job;
import com.domsd.jobmatcher.model.Worker;
import com.domsd.jobmatcher.service.matcher.criteria.filter.DriverLicenseCriterion;

public class DriverLicenseCriterionUnitTest {

    
    private DriverLicenseCriterion driverLicenseCriterion;
    
    @Before
    public void setup() {
        driverLicenseCriterion = new DriverLicenseCriterion();
    }
    
    
    @Test
    public void matched_jobDoesntRequireDriversLicense_workerDoesntHaveIt_shouldReturnTrue() {
        
        Boolean jobRequiresDriverLicense = false;
        Boolean workerHasDriverLicense   = false;
        
        boolean criterionMatched = buildAndRunBasicScenario(jobRequiresDriverLicense, workerHasDriverLicense);
        
        // Assertions
        assertTrue(criterionMatched);
    }

    @Test
    public void matched_jobDoesntRequireDriversLicense_workerHasIt_shouldReturnTrue() {
        
        Boolean jobRequiresDriverLicense = false;
        Boolean workerHasDriverLicense   = true;
        
        boolean criterionMatched = buildAndRunBasicScenario(jobRequiresDriverLicense, workerHasDriverLicense);
        
        // Assertions
        assertTrue(criterionMatched);
    }
    
    @Test
    public void matched_jobRequiresDriversLicense_workerDoesntHaveIt_shouldReturnFalse() {
        
        Boolean jobRequiresDriverLicense = true;
        Boolean workerHasDriverLicense   = false;
        
        boolean criterionMatched = buildAndRunBasicScenario(jobRequiresDriverLicense, workerHasDriverLicense);
        
        // Assertions
        assertFalse(criterionMatched);
    }

    @Test
    public void matched_jobRequireDriversLicense_workerHasIt_shouldReturnTrue() {
        
        Boolean jobRequiresDriverLicense = true;
        Boolean workerHasDriverLicense   = true;
        
        boolean criterionMatched = buildAndRunBasicScenario(jobRequiresDriverLicense, workerHasDriverLicense);
        
        // Assertions
        assertTrue(criterionMatched);
    }
    
    @Test
    public void matched_jobRequireDriversLicense_workerDriverLicenseIsNull_shouldReturnFalse() {
        
        Boolean jobRequiresDriverLicense = true;
        Boolean workerHasDriverLicense   = null;
        
        boolean criterionMatched = buildAndRunBasicScenario(jobRequiresDriverLicense, workerHasDriverLicense);
        
        // Assertions
        assertFalse(criterionMatched);
    }
    
    @Test
    public void matched_jobDoesntRequireDriversLicense_workerDriverLicenseIsNull_shouldReturnTrue() {
        
        Boolean jobRequiresDriverLicense = false;
        Boolean workerHasDriverLicense   = null;
        
        boolean criterionMatched = buildAndRunBasicScenario(jobRequiresDriverLicense, workerHasDriverLicense);
        
        // Assertions
        assertTrue(criterionMatched);
    }
    
    @Test
    public void matched_jobRequiresDriversLicenseIsNull_workerDriverLicenseIsNull_shouldReturnTrue() {
        
        Boolean jobRequiresDriverLicense = null;
        Boolean workerHasDriverLicense   = null;
        
        boolean criterionMatched = buildAndRunBasicScenario(jobRequiresDriverLicense, workerHasDriverLicense);
        
        // Assertions
        assertTrue(criterionMatched);
    }
    
    @Test
    public void matched_jobRequiresDriversLicenseIsNull_workerHasDriverLicense_shouldReturnTrue() {
        
        Boolean jobRequiresDriverLicense = null;
        Boolean workerHasDriverLicense   = true;
        
        boolean criterionMatched = buildAndRunBasicScenario(jobRequiresDriverLicense, workerHasDriverLicense);
        
        // Assertions
        assertTrue(criterionMatched);
    }
    
    @Test
    public void matched_jobRequiresDriversLicenseIsNull_workerDoesntHaveIt_shouldReturnTrue() {
        
        Boolean jobRequiresDriverLicense = null;
        Boolean workerHasDriverLicense   = false;
        
        boolean criterionMatched = buildAndRunBasicScenario(jobRequiresDriverLicense, workerHasDriverLicense);
        
        // Assertions
        assertTrue(criterionMatched);
    }
    
    private boolean buildAndRunBasicScenario(Boolean jobRequiresDriverLicense, Boolean workerHasDriverLicense) {
        
        // Configurations
        Job job = new Job();
        Worker worker = new Worker();
        
        job.setDriverLicenseRequired(jobRequiresDriverLicense);
        worker.setHasDriversLicense(workerHasDriverLicense);
        
        // Action
        boolean criterionMatched = driverLicenseCriterion.matched(worker, job);
        
        
        return criterionMatched;
    }
}
