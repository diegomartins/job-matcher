package com.domsd.jobmatcher.service.matcher;



import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.domsd.jobmatcher.model.Job;
import com.domsd.jobmatcher.model.Worker;
import com.domsd.jobmatcher.service.matcher.JobMatchCheckerServiceImpl;
import com.domsd.jobmatcher.service.matcher.criteria.filter.JobMatchCriterion;

public class JobMatchCheckerServiceUnitTest {

    @InjectMocks
    private JobMatchCheckerServiceImpl matcher;

    @Mock
    private JobMatchCriterion alwaysMatches;

    @Mock
    private JobMatchCriterion neverMatches;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        configureMocks();
    }

    @Test
    public void allCriteriaMatched_oneCriteria_matchesIt_shouldReturnTrue() {

        JobMatchCriterion[] criteria = { alwaysMatches };
        
        boolean allCriteriaMatched = buildAndRunBasicScenario(criteria);
        
        assertTrue(allCriteriaMatched);

    }
    
    @Test
    public void allCriteriaMatched_moreThanOneCriteria_matchesAll_shouldReturnTrue() {

        JobMatchCriterion[] criteria = { alwaysMatches, alwaysMatches };
 
        boolean allCriteriaMatched = buildAndRunBasicScenario(criteria);
        
        assertTrue(allCriteriaMatched);

    }
    
    @Test
    public void allCriteriaMatched_oneCriteria_doesntMatchIt_shouldReturnFalse() {

        JobMatchCriterion[] criteria = { neverMatches };
        
        boolean allCriteriaMatched = buildAndRunBasicScenario(criteria);
        
        assertFalse(allCriteriaMatched);

    }
    
    @Test
    public void allCriteriaMatched_moreThanOneCriteria_matchesNone_shouldReturnFalse() {

        JobMatchCriterion[] criteria = { neverMatches, neverMatches };
        
        boolean allCriteriaMatched = buildAndRunBasicScenario(criteria);
        
        assertFalse(allCriteriaMatched);

    }
    
    @Test
    public void allCriteriaMatched_moreThanOneCriteria_matchesAllButTheLast_shouldReturnFalse() {

        JobMatchCriterion[] criteria = { alwaysMatches, alwaysMatches, neverMatches };
        
        boolean allCriteriaMatched = buildAndRunBasicScenario(criteria);
        
        assertFalse(allCriteriaMatched);

    }
    
    @Test
    public void allCriteriaMatched_moreThanOneCriteria_matchesAllButTheFirst_shouldReturnFalse() {

        JobMatchCriterion[] criteria = { neverMatches, alwaysMatches, alwaysMatches };
        
        boolean allCriteriaMatched = buildAndRunBasicScenario(criteria);
        
        assertFalse(allCriteriaMatched);

    }
    
    @Test
    public void allCriteriaMatched_noCriteria_shouldReturnTrue() {

        JobMatchCriterion[] criteria = {  };
        
        boolean allCriteriaMatched = buildAndRunBasicScenario(criteria);
        
        assertTrue(allCriteriaMatched);
    }

    
    private boolean buildAndRunBasicScenario(JobMatchCriterion[] criteria) {
        
        Worker worker = new Worker();
        Job job = new Job();
        

        boolean allCriteriaMatched = matcher.allCriteriaMatched(criteria, worker, job);
        
        return allCriteriaMatched;
    }

    private void configureMocks() {

        Mockito.when(alwaysMatches.matched(Mockito.any(Worker.class), Mockito.any(Job.class))).thenReturn(true);
        Mockito.when(neverMatches.matched(Mockito.any(Worker.class), Mockito.any(Job.class))).thenReturn(false);
    }
}
