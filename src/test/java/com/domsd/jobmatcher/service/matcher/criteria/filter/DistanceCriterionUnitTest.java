package com.domsd.jobmatcher.service.matcher.criteria.filter;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.RETURNS_SELF;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.commons.lang3.RandomUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.domsd.jobmatcher.model.Job;
import com.domsd.jobmatcher.model.JobSearchAddress;
import com.domsd.jobmatcher.model.Location;
import com.domsd.jobmatcher.model.Worker;
import com.domsd.jobmatcher.service.geo.AbstractDistanceCalculator;
import com.domsd.jobmatcher.service.matcher.criteria.filter.DistanceCriterion;

public class DistanceCriterionUnitTest {

    private DistanceCriterion distanceCriterion;

    private AbstractDistanceCalculator distanceCalculator;

    private Double jobSearchLongitude;
    private Double jobSearchLatitude;

    private Double jobLongitude;
    private Double jobLatitude;

    @Before
    public void setup() {
        distanceCalculator = mock(AbstractDistanceCalculator.class, RETURNS_SELF);
        distanceCriterion = new DistanceCriterion(distanceCalculator);

        initializeDefaultCoordinates();
    }

    @Test
    public void matched_searchRangeIsHigherThanJobDistance_shouldReturnTrue() {

        Integer searchRange = 11;
        Double jobDistance = 10d;

        boolean criterionMatched = buildAndRunBasicScenario(jobDistance, searchRange);

        // Assertions
        assertTrue(criterionMatched);

        verifyCalculatorProperlyCalled();
    }

    @Test
    public void matched_searchRangeIsExactlyTheSameAsJobDistance_shouldReturnTrue() {

        Integer searchRange = 30;
        Double jobDistance = 30d;

        boolean criterionMatched = buildAndRunBasicScenario(jobDistance, searchRange);

        // Assertions
        assertTrue(criterionMatched);

        verifyCalculatorProperlyCalled();
    }

    @Test
    public void matched_searchRangeIsLowerThanTheAsJobDistance_byLessThanPoint5_shouldReturnTrue() {

        Integer searchRange = 10;
        Double jobDistance = 10.499d;

        boolean criterionMatched = buildAndRunBasicScenario(jobDistance, searchRange);

        // Assertions
        assertTrue(criterionMatched);

        verifyCalculatorProperlyCalled();
    }

    @Test
    public void matched_searchRangeIsLowerThanTheAsJobDistance_by1_shouldReturnFalse() {

        Integer searchRange = 10;
        Double jobDistance = 11d;

        boolean criterionMatched = buildAndRunBasicScenario(jobDistance, searchRange);

        // Assertions
        assertFalse(criterionMatched);

        verifyCalculatorProperlyCalled();
    }

    @Test
    public void matched_searchRangeIsLowerThanTheAsJobDistance_byPoint5_shouldReturnFalse() {

        Integer searchRange = 10;
        Double jobDistance = 10.5d;

        boolean criterionMatched = buildAndRunBasicScenario(jobDistance, searchRange);

        // Assertions
        assertFalse(criterionMatched);

        verifyCalculatorProperlyCalled();
    }

    @Test
    public void matched_jobSearchAddressIsNull_shouldReturnTrue() {

        JobSearchAddress searchAddress = null;
        Location jobLocation           = configureDefaultJobLocation();

        boolean criterionMatched = buildAndRunScenarioWithNullables(searchAddress, jobLocation);

        // Assertions
        assertTrue(criterionMatched);
    }

    @Test
    public void matched_jobSearchAddressWasPartiallyDefined_latitudeNull_shouldReturnTrue() {

        JobSearchAddress searchAddress = configureDefaultJobSearchAddress(30);
        searchAddress.setLatitude(null);
        
        Location jobLocation = configureDefaultJobLocation();

        boolean criterionMatched = buildAndRunScenarioWithNullables(searchAddress, jobLocation);

        // Assertions
        assertTrue(criterionMatched);
    }
    
    @Test
    public void matched_jobSearchAddressWasPartiallyDefined_longitudeNull_shouldReturnTrue() {

        JobSearchAddress searchAddress = configureDefaultJobSearchAddress(30);
        searchAddress.setLongitude(null);
        
        Location jobLocation = configureDefaultJobLocation();

        boolean criterionMatched = buildAndRunScenarioWithNullables(searchAddress, jobLocation);

        // Assertions
        assertTrue(criterionMatched);
    }
    
    @Test
    public void matched_jobSearchAddressWasPartiallyDefined_maxDistanceIsNull_shouldReturnTrue() {

        JobSearchAddress searchAddress = configureDefaultJobSearchAddress(30);
        searchAddress.setMaxJobDistance(null);
        
        Location jobLocation = configureDefaultJobLocation();

        boolean criterionMatched = buildAndRunScenarioWithNullables(searchAddress, jobLocation);

        // Assertions
        assertTrue(criterionMatched);
    }

    @Test
    public void matched_jobSearchAddressWasCompletelyDefined_jobLocationIsNull_shouldReturnFalse() {

        JobSearchAddress searchAddress = configureDefaultJobSearchAddress(30);
        Location jobLocation           = null;

        boolean criterionMatched = buildAndRunScenarioWithNullables(searchAddress, jobLocation);

        // Assertions
        assertFalse(criterionMatched);
    }
    
    
    @Test
    public void matched_jobSearchAddressWasCompletelyDefined_jobLocationWasPartiallyDefined_latitudeNull_shouldReturnFalse() {

        JobSearchAddress searchAddress = configureDefaultJobSearchAddress(30);
        Location jobLocation           = configureDefaultJobLocation();
        jobLocation.setLatitude(null);
        
        boolean criterionMatched = buildAndRunScenarioWithNullables(searchAddress, jobLocation);

        // Assertions
        assertFalse(criterionMatched);
    }
    
    
    @Test
    public void matched_jobSearchAddressWasCompletelyDefined_jobLocationWasPartiallyDefined_longitudeNull_shouldReturnFalse() {

        JobSearchAddress searchAddress = configureDefaultJobSearchAddress(30);
        Location jobLocation           = configureDefaultJobLocation();
        jobLocation.setLongitude(null);

        boolean criterionMatched = buildAndRunScenarioWithNullables(searchAddress, jobLocation);

        // Assertions
        assertFalse(criterionMatched);
    }
    
    private boolean buildAndRunBasicScenario(Double actualDistance, Integer maxJobDistance) {

        JobSearchAddress jobSearchAddress = configureDefaultJobSearchAddress(maxJobDistance);
        Location jobLocation = configureDefaultJobLocation();

        Worker worker = new Worker();
        Job job = new Job();

        job.setLocation(jobLocation);
        worker.setJobSearchAddress(jobSearchAddress);

        // Isolating the dependency with the DistanceCalculator
        when(distanceCalculator.calculate()).thenReturn(actualDistance);

        boolean criterionMatched = distanceCriterion.matched(worker, job);

        return criterionMatched;
    }

    private boolean buildAndRunScenarioWithNullables(JobSearchAddress searchAddress, Location jobLocation) {

        Worker worker = new Worker();
        Job job = new Job();

        job.setLocation(jobLocation);
        worker.setJobSearchAddress(searchAddress);

        boolean criterionMatched = distanceCriterion.matched(worker, job);

        return criterionMatched;
    }

    private void verifyCalculatorProperlyCalled() {

        Mockito.verify(distanceCalculator).from(jobSearchLatitude, jobSearchLongitude);
        Mockito.verify(distanceCalculator).to(jobLatitude, jobLongitude);
        Mockito.verify(distanceCalculator).calculate();

    }

    private JobSearchAddress configureDefaultJobSearchAddress(Integer maxJobDistance) {

        JobSearchAddress jobSearchAddress = new JobSearchAddress();
        jobSearchAddress.setLatitude(jobSearchLatitude);
        jobSearchAddress.setLongitude(jobSearchLongitude);
        jobSearchAddress.setMaxJobDistance(maxJobDistance);

        return jobSearchAddress;
    }

    private Location configureDefaultJobLocation() {

        Location location = new Location();
        location.setLatitude(jobLatitude);
        location.setLongitude(jobLongitude);

        return location;
    }

    private void initializeDefaultCoordinates() {

        jobSearchLongitude = RandomUtils.nextDouble(0, 100);
        jobSearchLatitude = RandomUtils.nextDouble(0, 100);

        jobLongitude = RandomUtils.nextDouble(0, 100);
        jobLatitude = RandomUtils.nextDouble(0, 100);
    }

}
