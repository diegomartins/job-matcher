package com.domsd.jobmatcher.service.matcher.criteria.sorting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Comparator;

import org.junit.Before;
import org.junit.Test;

import com.domsd.jobmatcher.model.Job;

public class BillRateSortingCriterionUnitTest {

    private static final String BILL_RATE_3_20 = "$3.20";
    private static final String BILL_RATE_10_40 = "$10.40";
    
    private BillRateSortingCriterion billRateSortingCriterion;
    
    @Before
    public void setup() {
        billRateSortingCriterion = new BillRateSortingCriterion();
    }
    
    @Test
    public void toComparator_sameBillRateValue_shouldReturn0() {
        
        String billRate1 = BILL_RATE_3_20;
        String billRate2 = BILL_RATE_3_20;
        
        int result = buildAndRunBasicScenario(billRate1, billRate2);
        
        shouldBeZero(result);
    }
    
    @Test
    public void toComparator_billRate1LowerThanBillRate2_shouldReturnPositiveNumber() {
        
        String billRate1 = BILL_RATE_3_20;
        String billRate2 = BILL_RATE_10_40;
        
        int result = buildAndRunBasicScenario(billRate1, billRate2);
        
        shouldBePositive(result);
    }

    
    @Test
    public void toComparator_billRate1HigherThanBillRate2_shouldReturnNegativeNumber() {
        
        String billRate1 = BILL_RATE_10_40;
        String billRate2 = BILL_RATE_3_20;
        
        int result = buildAndRunBasicScenario(billRate1, billRate2);
        
        shouldBeNegative(result);
    }
    
    @Test
    public void toComparator_billRatesAreNull_shouldReturn0() {
        
        String billRate1 = null;
        String billRate2 = null;
        
        int result = buildAndRunBasicScenario(billRate1, billRate2);
        
        shouldBeZero(result);
    }
    
    @Test
    public void toComparator_billRatesAreEmpty_shouldReturn0() {
        
        String billRate1 = "";
        String billRate2 = "";
        
        int result = buildAndRunBasicScenario(billRate1, billRate2);
        
        shouldBeZero(result);
    }


    @Test
    public void toComparator_onlyBillRate1IsEmpty_shouldReturnNegativeNumber() {
        
        String billRate1 = "";
        String billRate2 = BILL_RATE_3_20;
        
        int result = buildAndRunBasicScenario(billRate1, billRate2);
        
        shouldBeNegative(result);
    }
    
    @Test
    public void toComparator_onlyBillRate2IsEmpty_shouldReturnPositiveNumber() {
        
        String billRate1 = BILL_RATE_3_20;
        String billRate2 = "";
        
        int result = buildAndRunBasicScenario(billRate1, billRate2);
        
        shouldBePositive(result);
    }
    
    @Test
    public void toComparator_onlyBillRate1IsNull_shouldReturnNegativeNumber() {
        
        String billRate1 = null;
        String billRate2 = BILL_RATE_3_20;
        
        int result = buildAndRunBasicScenario(billRate1, billRate2);
        
        shouldBeNegative(result);
    }
    
    @Test
    public void toComparator_onlyBillRate2IsNull_shouldReturnPositiveNumber() {
        
        String billRate1 = BILL_RATE_3_20;
        String billRate2 = null;
        
        int result = buildAndRunBasicScenario(billRate1, billRate2);
        
        shouldBePositive(result);
    }
    
    
    public int buildAndRunBasicScenario(String billRate1, String billRate2) {
        
        Job job1 = new Job();
        job1.setBillRate(billRate1);
        Job job2 = new Job();
        job2.setBillRate(billRate2);
        
        
        Comparator<Job> comparator = billRateSortingCriterion.toComparator();
        int result = comparator.compare(job1, job2);
        
        return result;
    }
    
    private void shouldBeZero(int result) {
        assertEquals(0, result);
    }
    
    private void shouldBeNegative(int result) {
        assertTrue(result < 0);
    }
    
    private void shouldBePositive(int result) {
        assertTrue(result > 0);
    }
    

}
