package com.domsd.jobmatcher.service.matcher.criteria.filter;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.domsd.jobmatcher.model.Job;
import com.domsd.jobmatcher.model.Worker;
import com.domsd.jobmatcher.service.matcher.criteria.filter.RequiredCertificatesCriterion;
import com.domsd.jobmatcher.testutils.TestUtils;

public class RequiredCertificatesCriterionUnitTest {
    
    
    private RequiredCertificatesCriterion requiredCertificatesCriterion;
    
    @Before
    public void setup() {
        requiredCertificatesCriterion = new RequiredCertificatesCriterion();
    }
    
    
    @Test
    public void matched_jobRequiresNoCertificates_workerHasNone_shouldReturnTrue() {
        
        Set<String> requiredCertificates = Collections.emptySet();
        Set<String> workerCertificates   = Collections.emptySet();
        
        boolean matchedTheCriterion = buildAndRunBasicScenario(requiredCertificates, workerCertificates);
        
        assertTrue(matchedTheCriterion);
    }
    
    @Test
    public void matched_jobRequiresNoCertificates_workerHasSome_shouldReturnTrue() {
        
        Set<String> requiredCertificates = Collections.emptySet();
        Set<String> workerCertificates   = TestUtils.asSet("A", "B", "C");
        
        boolean matchedTheCriterion = buildAndRunBasicScenario(requiredCertificates, workerCertificates);
        
        assertTrue(matchedTheCriterion);
    }
    
    @Test
    public void matched_jobRequiresOneCertificate_workerHasItAndSomeMore_shouldReturnTrue() {
        
        Set<String> requiredCertificates = TestUtils.asSet("B");
        Set<String> workerCertificates   = TestUtils.asSet("A", "B", "C");
        
        boolean matchedTheCriterion = buildAndRunBasicScenario(requiredCertificates, workerCertificates);
        
        assertTrue(matchedTheCriterion);
    }
    
    @Test
    public void matched_jobRequiresSomeCertificates_workerHasExactlyTheSame_shouldReturnTrue() {
        
        Set<String> requiredCertificates = TestUtils.asSet("B", "C", "A");
        Set<String> workerCertificates   = TestUtils.asSet("A", "B", "C");
        
        boolean matchedTheCriterion = buildAndRunBasicScenario(requiredCertificates, workerCertificates);
        
        assertTrue(matchedTheCriterion);
    }
    
    @Test
    public void matched_jobRequiresSomeCertificates_workerHasThemAllAndSomeMore_shouldReturnTrue() {
        
        Set<String> requiredCertificates = TestUtils.asSet("B", "C", "A");
        Set<String> workerCertificates   = TestUtils.asSet("D", "A", "B", "C", "E");
        
        boolean matchedTheCriterion = buildAndRunBasicScenario(requiredCertificates, workerCertificates);
        
        assertTrue(matchedTheCriterion);
    }
    
    @Test
    public void matched_jobRequiresSomeCertificates_workerHasAllButOne_shouldReturnFalse() {
        
        Set<String> requiredCertificates = TestUtils.asSet("B", "C", "A");
        Set<String> workerCertificates   = TestUtils.asSet("Z", "C", "A");
        
        boolean matchedTheCriterion = buildAndRunBasicScenario(requiredCertificates, workerCertificates);
        
        assertFalse(matchedTheCriterion);
    }
    
    @Test
    public void matched_jobRequiresSomeCertificates_workerHasNoneOfThem_shouldReturnFalse() {
        
        Set<String> requiredCertificates = TestUtils.asSet("B", "C", "A");
        Set<String> workerCertificates   = TestUtils.asSet("Z", "X", "Y");
        
        boolean matchedTheCriterion = buildAndRunBasicScenario(requiredCertificates, workerCertificates);
        
        assertFalse(matchedTheCriterion);
    }
    
    @Test
    public void matched_jobRequiresSomeCertificates_workerHasNoneAtAll_shouldReturnFalse() {
        
        Set<String> requiredCertificates = TestUtils.asSet("B", "C", "A");
        Set<String> workerCertificates   = Collections.emptySet();
        
        boolean matchedTheCriterion = buildAndRunBasicScenario(requiredCertificates, workerCertificates);
        
        assertFalse(matchedTheCriterion);
    }
    
    @Test
    public void matched_jobCertificatesIsNull_workerHasNoneAtAll_shouldReturnTrue() {
        
        Set<String> requiredCertificates = null;
        Set<String> workerCertificates   = Collections.emptySet();
        
        boolean matchedTheCriterion = buildAndRunBasicScenario(requiredCertificates, workerCertificates);
        
        assertTrue(matchedTheCriterion);
    }
    
    @Test
    public void matched_jobCertificatesIsNull_workerCertificatesIsNull_shouldReturnTrue() {
        
        Set<String> requiredCertificates = null;
        Set<String> workerCertificates   = null;
        
        boolean matchedTheCriterion = buildAndRunBasicScenario(requiredCertificates, workerCertificates);
        
        assertTrue(matchedTheCriterion);
    }
    
    @Test
    public void matched_jobRequiresSomeCertificates_workerCertificatesIsNull_shouldReturnFalse() {
        
        Set<String> requiredCertificates = TestUtils.asSet("A", "B");
        Set<String> workerCertificates   = null;
        
        boolean matchedTheCriterion = buildAndRunBasicScenario(requiredCertificates, workerCertificates);
        
        assertFalse(matchedTheCriterion);
    }
    
    private boolean buildAndRunBasicScenario(Set<String> requiredCertificates, Set<String> workerCertificates) {
        
        // Configurations
        Job job = new Job();
        Worker worker = new Worker();
        
        job.setRequiredCertificates(requiredCertificates);
        worker.setCertificates(workerCertificates);
        
        // Action
        boolean criterionMatched = requiredCertificatesCriterion.matched(worker, job);
        
        
        return criterionMatched;
    }
    

}
