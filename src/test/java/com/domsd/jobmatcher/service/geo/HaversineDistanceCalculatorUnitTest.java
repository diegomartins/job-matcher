package com.domsd.jobmatcher.service.geo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.domsd.jobmatcher.testutils.TestUtils;

public class HaversineDistanceCalculatorUnitTest {

    private AbstractDistanceCalculator distanceCalculator;
    
    @Before
    public void setup() {
        distanceCalculator = new HaversineDistanceCalculator();
    }
    
    @Test
    public void calculateDistance_sameCoordinate_shouldReturn0() {
        
        double distance = distanceCalculator.from(49.782281, 13.971284)
                                            .to(49.782281, 13.971284)
                                            .calculate();
        
        Assert.assertEquals(0, distance, 0.00001);
    }
    
    /*
     * The values used for this calculation were taken from Google Maps
     */
    @Test
    public void calculateDistance_differentCoordinates_shouldReturnCorrectDistance() {
        
        double distance = distanceCalculator.from(-33.880180, 151.210747)
                                            .to(-33.8677631, 151.2070441)
                                            .calculate();
        
        Assert.assertEquals(1.42, distance, 0.005);
    }
    

    @Test
    public void calculateDistance_didntSetSourcePoint_shouldThrowException() {
        
        TestUtils.callExpectingException(() -> {
            
            distanceCalculator.to(-33.8677631, 151.2070441)
                               .calculate();
        
        }, IllegalArgumentException.class);
     
    }
    
    @Test
    public void calculateDistance_didntSetDestinationPoint_shouldThrowException() {
        
        TestUtils.callExpectingException(() -> {
            
            distanceCalculator.from(-33.880180, 151.210747)
                              .calculate();
        
        }, IllegalArgumentException.class);
     
    }
}
