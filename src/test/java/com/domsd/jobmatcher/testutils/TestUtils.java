package com.domsd.jobmatcher.testutils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.HashSet;
import java.util.Set;

import com.domsd.jobmatcher.exception.ApplicationException;
import com.domsd.jobmatcher.exception.ErrorMessage;

public class TestUtils {

	
	public static void callExpectingApplicationException(Runnable o, ErrorMessage message) {

		try {

			o.run();

		} catch (ApplicationException e) {

			assertEquals(message.getMessage(), e.getMessage());
			return;

		} catch (Exception e2) {
			
			fail("Expected exception with error \"" + message + "\" " +
				 "but got another exception instead. (" + e2.getClass() + ")");
		}

		fail("Expected exception with error \"" + message + "\" but got none instead.");
	}
	
	
	public static void callExpectingException(Runnable o, Class<?> exceptionClass) {

		try {

			o.run();

		} catch (Exception e) {

			String failureMessage = "Expected exception of type " + exceptionClass.getCanonicalName() +
						            " but got another instead: " + e.getClass().getCanonicalName();
			
			assertEquals(failureMessage, exceptionClass.getCanonicalName(), e.getClass().getCanonicalName());
			return;
		}

		fail("Expected untreated exception but got none instead.");
	}
	
	
	@SafeVarargs
	public static <T> Set<T> asSet(T... elements) {
	    
	    Set<T> set = new HashSet<>();
	    
	    for (T element : elements) {
	        set.add(element);
        }
	    
	    return set;
	}
	
}
