package com.domsd.jobmatcher.util;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;

import com.domsd.jobmatcher.testutils.TestUtils;

public class CurrencyConverterUnitTest {

    @Test
    public void toBigDecimal_properlyFormatted_shouldReturnCorrectValue() {
 
        BigDecimal convertedValue = CurrencyConverter.toBigDecimal("$1.15");
        
        assertEquals(1.15d, convertedValue.doubleValue(), 0.001);
    }
    
    @Test
    public void toBigDecimal_noCents_shouldReturnCorrectValue() {
 
        BigDecimal convertedValue = CurrencyConverter.toBigDecimal("$1");
        
        assertEquals(1d, convertedValue.doubleValue(), 0.001);
    }
    
    @Test
    public void toBigDecimal_onlyCents_shouldReturnCorrectValue() {
 
        BigDecimal convertedValue = CurrencyConverter.toBigDecimal("$.1");
        
        assertEquals(0.1d, convertedValue.doubleValue(), 0.001);
    }
    
    @Test
    public void toBigDecimal_differentPrefix_shouldThrowException() {
 
        TestUtils.callExpectingException(() -> {
            
            CurrencyConverter.toBigDecimal("U$1.15");
            
        }, NumberFormatException.class);

    }
    
    @Test
    public void toBigDecimal_null_shouldThrowException() {
 
        TestUtils.callExpectingException(() -> {
            
            CurrencyConverter.toBigDecimal(null);
            
        }, RuntimeException.class);

    }
}
