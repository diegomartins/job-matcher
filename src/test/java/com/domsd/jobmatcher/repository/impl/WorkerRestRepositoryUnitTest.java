package com.domsd.jobmatcher.repository.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.apache.commons.lang3.RandomUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.domsd.jobmatcher.model.Worker;
import com.domsd.jobmatcher.rest.RestClient;
import com.domsd.jobmatcher.service.configuration.ConfigurationService;

public class WorkerRestRepositoryUnitTest {

    @InjectMocks
    private WorkerRestRepository workerRestRepository;

    @Mock
    private RestClient restClient;

    @Mock
    private ConfigurationService configurationService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    
    /* -----------------------------------------------
     *             retrieveAllWorkers()
     * -----------------------------------------------
     */
    
    @Test
    public void retrieveAllWorkers_ifAPIReturnsNull_shouldReturnEmptyList() {

        // Configurations
        when(restClient.get(any(), any())).thenReturn(null);

        // Action
        List<Worker> allWorkers = workerRestRepository.retrieveAllWorkers();

        // Assertions
        assertNotNull(allWorkers);
        assertEquals(0, allWorkers.size());
    }

    @Test
    public void retrieveAllWorkers_shouldCallTheCorrectAPI() {

        // Configurations
        String api = "job.api";
        when(configurationService.allWorkersRestAPI()).thenReturn(api);

        // Action
        workerRestRepository.retrieveAllWorkers();

        // Assertions
        verify(restClient).get(api, Worker[].class);
    }

    @Test
    public void retrieveAllWorkers_ifAPIReturnsNonEmptyArray_shouldReturnAsList() {

        // Configurations
        Worker worker1 = mock(Worker.class);
        Worker worker2 = mock(Worker.class);
        
        Worker[] workers = { worker1, worker2 };
        when(restClient.get(any(), any())).thenReturn(workers);

        // Action
        List<Worker> allWorkers = workerRestRepository.retrieveAllWorkers();

        // Assertions
        assertNotNull(allWorkers);
        assertEquals(2, allWorkers.size());
        assertEquals(worker1, allWorkers.get(0));
        assertEquals(worker2, allWorkers.get(1));
    }
    
    
    /* -----------------------------------------------
     *         retrieveWorker(Long workerId)
     * -----------------------------------------------
     */
    //TODO: Complete
    @Test
    public void retrieveWorker_workerExists_shouldReturnWorker() {
        
        // Configurations
        Long workerId = RandomUtils.nextLong(0, 100);
        
        // Action
        Worker worker = workerRestRepository.retrieveWorker(workerId);
        
        // Assertions
        
    }
    
    
}
