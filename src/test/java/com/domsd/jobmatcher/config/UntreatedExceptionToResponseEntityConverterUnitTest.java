package com.domsd.jobmatcher.config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.domsd.jobmatcher.exception.ErrorMessage;
import com.domsd.jobmatcher.model.Error;
import com.domsd.jobmatcher.service.configuration.ConfigurationService;

public class UntreatedExceptionToResponseEntityConverterUnitTest {

    private static final String MESSAGE = "MESSAGE";

    @Mock
    private ConfigurationService configurationService;

    @InjectMocks
    private UntreatedExceptionToResponseEntityConverter untreatedExceptionToResponseEntityConverter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void convert_dontLogDebugInfo_shouldReturnResponseWithProperBodyAndStatus_noDebugInfo() {

        when(configurationService.includeDebugInformationOnErrorResponses()).thenReturn(false);
        RuntimeException exception = buildFullUntreatedException();

        ResponseEntity<Error> response = untreatedExceptionToResponseEntityConverter.convert(exception);

        assertNotNull(response);

        Error body = response.getBody();

        performBasicAssertions(response, body);
    }

    @Test
    public void convert_logDebugInfo_shouldReturnResponseWithProperBodyAndStatus_withDebugInfo() {

        when(configurationService.includeDebugInformationOnErrorResponses()).thenReturn(true);
        RuntimeException exception = buildFullUntreatedException();

        ResponseEntity<Error> response = untreatedExceptionToResponseEntityConverter.convert(exception);

        assertNotNull(response);

        Error body = response.getBody();

        performBasicAssertions(response, body);
        assertEquals(MESSAGE, body.getDebugInfo());
    }

    private RuntimeException buildFullUntreatedException() {

        RuntimeException exception = new RuntimeException(MESSAGE);

        return exception;
    }

    private void performBasicAssertions(ResponseEntity<Error> response, Error body) {

        assertNotNull(body);
        assertEquals(response.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
        assertEquals(ErrorMessage.UNEXPECTED_ERROR.getMessage(), body.getMessage());

    }

}
