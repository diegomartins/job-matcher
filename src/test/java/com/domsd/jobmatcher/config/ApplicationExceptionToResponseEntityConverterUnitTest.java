package com.domsd.jobmatcher.config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.domsd.jobmatcher.exception.ApplicationException;
import com.domsd.jobmatcher.exception.ErrorMessage;
import com.domsd.jobmatcher.model.Error;

public class ApplicationExceptionToResponseEntityConverterUnitTest {
   
    @InjectMocks
    private ApplicationExceptionToResponseEntityConverter applicationExceptionToResponseEntityConverter;
    
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void convert_shouldReturnResponseWithProperBodyAndStatus() {
        
        ApplicationException exception = new ApplicationException(ErrorMessage.WORKER_NOT_FOUND);
        
        ResponseEntity<Error> response = applicationExceptionToResponseEntityConverter.convert(exception);
        
        assertNotNull(response);
        
        Error body = response.getBody();
        
        assertNotNull(body);
        assertEquals(ErrorMessage.WORKER_NOT_FOUND.getMessage(), body.getMessage());
        assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }
    
}
