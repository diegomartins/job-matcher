package com.domsd.jobmatcher.config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.domsd.jobmatcher.exception.RestClientException;
import com.domsd.jobmatcher.model.Error;
import com.domsd.jobmatcher.service.configuration.ConfigurationService;

public class RestClientExceptionToResponseEntityConverterUnitTest {
   
    private static final String MESSAGE = "MESSAGE";
    private static final String DEBUG_MESSAGE = "DEBUG_MESSAGE";
    

    @Mock
    private ConfigurationService configurationService;
    
    @InjectMocks
    private RestClientExceptionToResponseEntityConverter restClientExceptionToResponseEntityConverter;
    
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void convert_dontLogDebugInfo_shouldReturnResponseWithProperBodyAndStatus_noDebugInfo() {
        
        when(configurationService.includeDebugInformationOnErrorResponses()).thenReturn(false);
        RestClientException exception = buildFullRestClientException();

        ResponseEntity<Error> response = restClientExceptionToResponseEntityConverter.convert(exception);
        
        assertNotNull(response);
        
        Error body = response.getBody();
        
        performBasicAssertions(response, body);
        assertNull(body.getDebugInfo());
    }

    
    @Test
    public void convert_logDebugInfo_shouldReturnResponseWithProperBodyAndStatus_withDebugInfo() {
        
        when(configurationService.includeDebugInformationOnErrorResponses()).thenReturn(true);
        RestClientException exception = buildFullRestClientException();

        ResponseEntity<Error> response = restClientExceptionToResponseEntityConverter.convert(exception);
        
        assertNotNull(response);
        
        Error body = response.getBody();
        
        performBasicAssertions(response, body);
        assertEquals(DEBUG_MESSAGE, body.getDebugInfo());
    }

    private RestClientException buildFullRestClientException() {
        
        Exception cause = mock(Exception.class);
        
        RestClientException exception = new RestClientException(cause);
        exception.setMessage(MESSAGE);
        exception.setDebugMessage(DEBUG_MESSAGE);
        
        return exception;
    }

    private void performBasicAssertions(ResponseEntity<Error> response, Error body) {
        
        assertNotNull(body);
        assertEquals(MESSAGE, body.getMessage());
        assertEquals(response.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
    
    }
    
}
