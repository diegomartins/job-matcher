package com.domsd.jobmatcher.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JobSearchAddress {
    
    //TODO: CONSIDER UNIT WHEN CALCULATING THE DISTANCES
    @JsonProperty("unit")
    private String unit;
    
    @JsonProperty("maxJobDistance")
    private Integer maxJobDistance;
    
    @JsonProperty("longitude")
    private Double longitude;
    
    @JsonProperty("latitude")
    private Double latitude;

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getMaxJobDistance() {
        return maxJobDistance;
    }

    public void setMaxJobDistance(Integer maxJobDistance) {
        this.maxJobDistance = maxJobDistance;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

}