package com.domsd.jobmatcher.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Availability {

    @JsonProperty("title")
    private String title;
    
    @JsonProperty("dayIndex")
    private Integer dayIndex;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getDayIndex() {
        return dayIndex;
    }

    public void setDayIndex(Integer dayIndex) {
        this.dayIndex = dayIndex;
    }

}