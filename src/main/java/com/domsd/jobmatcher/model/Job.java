package com.domsd.jobmatcher.model;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Job {

    @JsonProperty("jobId")
    private Long id;

    @JsonProperty("driverLicenseRequired")
    private Boolean driverLicenseRequired;
    
    @JsonProperty("requiredCertificates")
    private Set<String> requiredCertificates;
    
    @JsonProperty("location")
    private Location location;
    
    @JsonProperty("billRate")
    private String billRate;
    
    @JsonProperty("workersRequired")
    private Integer workersRequired;
    
    @JsonProperty("startDate")
    private String startDate;
    
    @JsonProperty("about")
    private String about;
    
    @JsonProperty("jobTitle")
    private String jobTitle;
    
    @JsonProperty("company")
    private String company;
    
    @JsonProperty("guid")
    private String guid;
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public Boolean getDriverLicenseRequired() {
        return driverLicenseRequired;
    }

    public void setDriverLicenseRequired(Boolean driverLicenseRequired) {
        this.driverLicenseRequired = driverLicenseRequired;
    }

    public Set<String> getRequiredCertificates() {
        return requiredCertificates;
    }

    public void setRequiredCertificates(Set<String> requiredCertificates) {
        this.requiredCertificates = requiredCertificates;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getBillRate() {
        return billRate;
    }

    public void setBillRate(String billRate) {
        this.billRate = billRate;
    }

    public Integer getWorkersRequired() {
        return workersRequired;
    }

    public void setWorkersRequired(Integer workersRequired) {
        this.workersRequired = workersRequired;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

}
