package com.domsd.jobmatcher.config;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.stereotype.Service;

import com.domsd.jobmatcher.exception.ErrorMessage;
import com.domsd.jobmatcher.model.Error;
import com.domsd.jobmatcher.service.configuration.ConfigurationService;

@Service
public class UntreatedExceptionToResponseEntityConverter implements ExceptionToResponseEntityConverter<Exception> {

    @Inject
    private ConfigurationService configurationService;
    
    
    public ResponseEntity<Error> convert(Exception e) {
        
        Error error = new Error();
        error.setMessage(ErrorMessage.UNEXPECTED_ERROR.getMessage());
        
        if(configurationService.includeDebugInformationOnErrorResponses()) {
            error.setDebugInfo(e.getMessage());
        }
        
        
        BodyBuilder responseBuilder = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR);
        ResponseEntity<Error> response = responseBuilder.body(error);
        
        return response;
    }
}
