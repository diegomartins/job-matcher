package com.domsd.jobmatcher.config;

import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.stereotype.Service;

import com.domsd.jobmatcher.exception.ApplicationException;
import com.domsd.jobmatcher.model.Error;

@Service
public class ApplicationExceptionToResponseEntityConverter implements ExceptionToResponseEntityConverter<ApplicationException> {
   
    public ResponseEntity<Error> convert(ApplicationException e) {
        
        Error error = new Error();
        error.setMessage(e.getMessage());
        
        //TODO: Make the exception more flexible so we can set the proper status instead of bad request
        BodyBuilder responseBuilder = ResponseEntity.badRequest();
        ResponseEntity<Error> response = responseBuilder.body(error);
        
        return response;
    }
}
