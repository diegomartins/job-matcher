package com.domsd.jobmatcher.config;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.domsd.jobmatcher.exception.ApplicationException;
import com.domsd.jobmatcher.exception.RestClientException;
import com.domsd.jobmatcher.model.Error;
import com.domsd.jobmatcher.service.configuration.ConfigurationService;

@ControllerAdvice
public class GlobalExceptionHandler {

    private static Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @Inject
    private ConfigurationService configurationService;

    @Inject
    private RestClientExceptionToResponseEntityConverter restClientExceptionToResponseEntityConverter;

    @Inject
    private ApplicationExceptionToResponseEntityConverter applicationExceptionToResponseEntityConverter;
    
    @Inject
    private UntreatedExceptionToResponseEntityConverter untreatedExceptionToResponseEntityConverter;

    
    @ExceptionHandler(ApplicationException.class)
    public ResponseEntity<Error> handleException(ApplicationException ex) {
        return applicationExceptionToResponseEntityConverter.convert(ex);
    }

    @ExceptionHandler(RestClientException.class)
    public ResponseEntity<Error> handleException(RestClientException ex) {
        return restClientExceptionToResponseEntityConverter.convert(ex);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Error> handleException(Exception ex) {

        if (configurationService.logUnknownExceptions()) {
            log.error(ex.getMessage(), ex);
        }

        return untreatedExceptionToResponseEntityConverter.convert(ex);
    }

}
