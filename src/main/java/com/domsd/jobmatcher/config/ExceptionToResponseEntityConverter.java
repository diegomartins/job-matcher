package com.domsd.jobmatcher.config;

import org.springframework.http.ResponseEntity;

import com.domsd.jobmatcher.model.Error;

public interface ExceptionToResponseEntityConverter<T extends Exception> {

    ResponseEntity<Error> convert(T exception);
}
