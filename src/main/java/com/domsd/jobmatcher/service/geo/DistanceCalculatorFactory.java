package com.domsd.jobmatcher.service.geo;

import org.springframework.stereotype.Component;

@Component
public class DistanceCalculatorFactory {
    
    public static AbstractDistanceCalculator getDefault() {
        return new HaversineDistanceCalculator();
    }
}
