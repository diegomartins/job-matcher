package com.domsd.jobmatcher.service.geo;

import com.spatial4j.core.distance.DistanceUtils;

public class HaversineDistanceCalculator extends AbstractDistanceCalculator {

    HaversineDistanceCalculator() {

    }

    /**
     * Calculates the distance in KM between the points defined on this
     * instance, using the Haversine formula.
     * 
     * @return The distance in KM between the two points, as determined by the
     *         Haversine formula, in radians.
     */
    public double calculate() {
        
        validateParameters();
        
        double distanceInRad = DistanceUtils.distLawOfCosinesRAD(Math.toRadians(latitudeX), Math.toRadians(longitudeX), 
                                                                 Math.toRadians(latitudeY), Math.toRadians(longitudeY));
        
        double distanceInKM = DistanceUtils.radians2Dist(distanceInRad, DistanceUtils.EARTH_MEAN_RADIUS_KM);
            
        return distanceInKM;
    }
    

}
