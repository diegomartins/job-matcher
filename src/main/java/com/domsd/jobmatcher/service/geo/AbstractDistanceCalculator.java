package com.domsd.jobmatcher.service.geo;

public abstract class AbstractDistanceCalculator {

    protected Double latitudeX;
    protected Double longitudeX;

    protected Double latitudeY;
    protected Double longitudeY;

    AbstractDistanceCalculator() {

    }

    public abstract double calculate();
    
    
    /**
     * Defines the point of origin to calculate the distance.
     * 
     * @return This instance
     */
    public AbstractDistanceCalculator from(double latitude, double longitude) {
        this.latitudeX = latitude;
        this.longitudeX = longitude;

        return this;
    }

    /**
     * Defines the point of destination to calculate the distance.
     * 
     * @return This instance
     */
    public AbstractDistanceCalculator to(double latitude, double longitude) {
        this.latitudeY = latitude;
        this.longitudeY = longitude;

        return this;
    }
    
    protected void validateParameters() {
        
        if(latitudeX == null || longitudeX == null) {
            throw new IllegalArgumentException("Source point was not properly set.");
        }
        
        if(latitudeY == null || longitudeY == null) {
            throw new IllegalArgumentException("Destination point was not properly set.");
        }
    }

}
