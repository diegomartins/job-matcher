package com.domsd.jobmatcher.service.configuration;

public interface ConfigurationService {
    
    public String allJobsRestAPI();
    public String allWorkersRestAPI();
    
    boolean logUnknownExceptions();
    boolean includeDebugInformationOnErrorResponses();
    
	int maximumNumberOfJobMatches();
}
