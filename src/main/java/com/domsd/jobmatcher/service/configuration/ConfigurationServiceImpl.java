package com.domsd.jobmatcher.service.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ConfigurationServiceImpl implements ConfigurationService {

    @Value("${app.config.log_unknown_exceptions}")
    private boolean logUnknownExceptions;
    
    @Value("${rest.api.job.all}")
    private String allJobsAPI;
    
    @Value("${rest.api.worker.all}")
    private String allWorkersAPI;
    
    @Value("${app.config.include_debug_info_on_error}")
    private boolean includeDebugInformationOnErrorResponses;
    
    @Value("${app.config.max_number_of_job_matches}")
    private int maximumNumberOfJobMatches;
    
    public boolean logUnknownExceptions() {
        return this.logUnknownExceptions;
    }

    @Override
    public String allJobsRestAPI() {
        return allJobsAPI;
    }


    @Override
    public String allWorkersRestAPI() {
        return allWorkersAPI;
    }

    @Override
    public boolean includeDebugInformationOnErrorResponses() {
        return includeDebugInformationOnErrorResponses;
    }

    @Override
	public int maximumNumberOfJobMatches() {
		return maximumNumberOfJobMatches;
	}

	
}
