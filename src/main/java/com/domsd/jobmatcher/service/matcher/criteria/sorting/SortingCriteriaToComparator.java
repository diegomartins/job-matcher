package com.domsd.jobmatcher.service.matcher.criteria.sorting;

import java.util.Comparator;

import org.springframework.stereotype.Component;

import com.domsd.jobmatcher.model.Job;

@Component
public class SortingCriteriaToComparator {

    public Comparator<Job> toComparator(JobSortingCriterion[] jobSortingCriteria) {

        return new Comparator<Job>() {

            @Override
            public int compare(Job o1, Job o2) {

                for (JobSortingCriterion jobSortingCriterion : jobSortingCriteria) {

                    int result = jobSortingCriterion.toComparator().compare(o1, o2);

                    if (result != 0) {
                        return result;
                    }
                }

                return 0;
            }
        };
    }

}
