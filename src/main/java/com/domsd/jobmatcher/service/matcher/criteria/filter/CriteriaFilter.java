package com.domsd.jobmatcher.service.matcher.criteria.filter;

public interface CriteriaFilter<T, S> {

    boolean matched(T entityA, S entityB, FilterCriterion<T, S>[] criteria);
}
