package com.domsd.jobmatcher.service.matcher.criteria.sorting;

import java.util.Comparator;

public interface SortingCriterion<T> {
    
    Comparator<T> toComparator();
}
