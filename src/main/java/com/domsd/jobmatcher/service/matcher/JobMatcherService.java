package com.domsd.jobmatcher.service.matcher;

import java.util.List;

import com.domsd.jobmatcher.model.Job;

public interface JobMatcherService {

    List<Job> matches(Long workerId); 
}
