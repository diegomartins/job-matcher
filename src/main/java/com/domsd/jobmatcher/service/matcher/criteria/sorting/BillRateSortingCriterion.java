package com.domsd.jobmatcher.service.matcher.criteria.sorting;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import com.domsd.jobmatcher.model.Job;
import com.domsd.jobmatcher.util.CurrencyConverter;

public class BillRateSortingCriterion implements JobSortingCriterion {

    @Override
    public Comparator<Job> toComparator() {
        
        return new Comparator<Job>() {

            @Override
            public int compare(Job job1, Job job2) {
                
                String billRate1 = Optional.ofNullable(job1.getBillRate()).orElse("");
                String billRate2 = Optional.ofNullable(job2.getBillRate()).orElse("");
                
                if(billRate1.equals(billRate2)) {
                    return 0;
                }
                
                if(StringUtils.isEmpty(billRate1)) {
                    return -1;
                }
                
                if(StringUtils.isEmpty(billRate2)) {
                    return 1;
                }
                
                BigDecimal billRateAsNumber1 = CurrencyConverter.toBigDecimal(billRate1);
                BigDecimal billRateAsNumber2 = CurrencyConverter.toBigDecimal(billRate2);
                
                return billRateAsNumber2.compareTo(billRateAsNumber1);
                
            }
        };
    }

}
