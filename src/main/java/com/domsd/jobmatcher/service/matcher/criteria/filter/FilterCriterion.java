package com.domsd.jobmatcher.service.matcher.criteria.filter;

public interface FilterCriterion<T, V> {

    boolean matched(T entityA, V entityB);
}
