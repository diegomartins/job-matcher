package com.domsd.jobmatcher.service.matcher.criteria.sorting;

import com.domsd.jobmatcher.model.Job;

public interface JobSortingCriterion extends SortingCriterion<Job> {
    
}
