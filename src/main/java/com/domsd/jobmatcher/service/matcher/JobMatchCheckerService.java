package com.domsd.jobmatcher.service.matcher;

import com.domsd.jobmatcher.model.Job;
import com.domsd.jobmatcher.model.Worker;
import com.domsd.jobmatcher.service.matcher.criteria.filter.JobMatchCriterion;

public interface JobMatchCheckerService {

    boolean allCriteriaMatched(JobMatchCriterion[] criteria, Worker worker, Job job);
        
}
