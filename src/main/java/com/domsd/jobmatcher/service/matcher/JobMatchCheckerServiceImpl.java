package com.domsd.jobmatcher.service.matcher;

import org.springframework.stereotype.Service;

import com.domsd.jobmatcher.model.Job;
import com.domsd.jobmatcher.model.Worker;
import com.domsd.jobmatcher.service.matcher.criteria.filter.JobMatchCriterion;

@Service
public class JobMatchCheckerServiceImpl implements JobMatchCheckerService {

    public boolean allCriteriaMatched(JobMatchCriterion[] criteria, Worker worker, Job job) {

        for (JobMatchCriterion criterion : criteria) {

            if (!criterion.matched(worker, job)) {
                return false;
            }
        }

        return true;
    }
}
