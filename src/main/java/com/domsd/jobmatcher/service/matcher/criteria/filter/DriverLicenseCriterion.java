package com.domsd.jobmatcher.service.matcher.criteria.filter;

import static org.apache.commons.lang3.BooleanUtils.isTrue;

import com.domsd.jobmatcher.model.Job;
import com.domsd.jobmatcher.model.Worker;

public class DriverLicenseCriterion implements JobMatchCriterion {

    @Override
    public boolean matched(Worker worker, Job job) {
           
        boolean jobRequiresDriverLicense = isTrue(job.getDriverLicenseRequired());
        
        if(jobRequiresDriverLicense) {
            return isTrue(worker.getHasDriversLicense());
        }
        
        return true;
    }

}
