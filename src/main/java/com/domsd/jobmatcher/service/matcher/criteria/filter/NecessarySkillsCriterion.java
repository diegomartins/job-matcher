package com.domsd.jobmatcher.service.matcher.criteria.filter;

import java.util.Set;

import com.domsd.jobmatcher.model.Job;
import com.domsd.jobmatcher.model.Worker;

public class NecessarySkillsCriterion implements JobMatchCriterion {

    @Override
    public boolean matched(Worker worker, Job job) {

        String jobTitle = job.getJobTitle();
        Set<String> workerSkills = worker.getSkills();

        if (workerSkills == null) {
            return false;
        }

        boolean workerHasTheNecessarySkills = workerSkills.contains(jobTitle);

        return workerHasTheNecessarySkills;
    }

}
