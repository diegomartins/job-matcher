package com.domsd.jobmatcher.service.matcher;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.domsd.jobmatcher.exception.ApplicationException;
import com.domsd.jobmatcher.exception.ErrorMessage;
import com.domsd.jobmatcher.model.Job;
import com.domsd.jobmatcher.model.Worker;
import com.domsd.jobmatcher.repository.JobRepository;
import com.domsd.jobmatcher.repository.WorkerRepository;
import com.domsd.jobmatcher.service.configuration.ConfigurationService;
import com.domsd.jobmatcher.service.matcher.criteria.filter.DistanceCriterion;
import com.domsd.jobmatcher.service.matcher.criteria.filter.DriverLicenseCriterion;
import com.domsd.jobmatcher.service.matcher.criteria.filter.JobMatchCriterion;
import com.domsd.jobmatcher.service.matcher.criteria.filter.NecessarySkillsCriterion;
import com.domsd.jobmatcher.service.matcher.criteria.filter.RequiredCertificatesCriterion;
import com.domsd.jobmatcher.service.matcher.criteria.sorting.BillRateSortingCriterion;
import com.domsd.jobmatcher.service.matcher.criteria.sorting.DistanceSortingCriterion;
import com.domsd.jobmatcher.service.matcher.criteria.sorting.JobSortingCriterion;
import com.domsd.jobmatcher.service.matcher.criteria.sorting.SortingCriteriaToComparator;


@Service
public class JobMatcherServiceImpl implements JobMatcherService {
	
    @Inject
    private JobRepository jobRepository;
    
    @Inject
    private WorkerRepository workerRepository;
    
    @Inject
    private ConfigurationService configurationService;
    
    @Inject
    private JobMatchCheckerService matchChecker;
    
    @Inject
    private SortingCriteriaToComparator sortingCriteriaToComparator;
    
    
    public List<Job> matches(Long workerId) {
		
		validateParameter(workerId);
		
		Worker worker = findWorkerOrFail(workerId);
		
		List<Job> allJobs = jobRepository.retrieveAllJobs();
		
		List<Job> matched = findMatches(worker, allJobs);
		
		return matched;
		
	}

    private void validateParameter(Long workerId) {
        
        if(workerId == null) {
		    throw new ApplicationException(ErrorMessage.UNEXPECTED_NULL_IDENTIFIER);
		}
        
    }

    /*
     * TODO: Improve the sorting
     */
	private List<Job> findMatches(Worker worker, List<Job> allJobs) {
		
		JobMatchCriterion[] filterCriteria = { new DriverLicenseCriterion(), new NecessarySkillsCriterion(), 
		                                       new RequiredCertificatesCriterion(), new DistanceCriterion() };
		
		JobSortingCriterion[] sortingCriteria = { new BillRateSortingCriterion(), 
		                                          new DistanceSortingCriterion(worker.getJobSearchAddress()) };
		
		List<Job> matched = allJobs.stream()
				                   .filter((job) -> isAMatch(worker, job, filterCriteria))
				                   .sorted(sortingCriteriaToComparator.toComparator(sortingCriteria))
				                   .limit(configurationService.maximumNumberOfJobMatches())
				                   .collect(Collectors.toList());
		return matched;
	}

	private boolean isAMatch(Worker worker, Job job, JobMatchCriterion[] criteria) {
		
		return matchChecker.allCriteriaMatched(criteria, worker, job);
	}
	
	
    private Worker findWorkerOrFail(Long workerId) {
        
        Worker worker = workerRepository.retrieveWorker(workerId);
        
		if(worker == null) {
		    throw new ApplicationException(ErrorMessage.WORKER_NOT_FOUND);
		}
		
		return worker;
    }
	
}
