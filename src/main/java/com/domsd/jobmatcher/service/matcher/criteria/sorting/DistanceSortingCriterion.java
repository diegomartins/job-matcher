package com.domsd.jobmatcher.service.matcher.criteria.sorting;

import java.util.Comparator;

import com.domsd.jobmatcher.model.Job;
import com.domsd.jobmatcher.model.JobSearchAddress;
import com.domsd.jobmatcher.model.Location;
import com.domsd.jobmatcher.service.geo.AbstractDistanceCalculator;
import com.domsd.jobmatcher.service.geo.DistanceCalculatorFactory;
import com.domsd.jobmatcher.util.NullUtils;

public class DistanceSortingCriterion implements JobSortingCriterion {
    
    private JobSearchAddress searchAddress;
    
    private AbstractDistanceCalculator distanceCalculator;
    
    
    public DistanceSortingCriterion(JobSearchAddress searchAddress) {
        this(searchAddress, DistanceCalculatorFactory.getDefault());
    }
    
    public DistanceSortingCriterion(JobSearchAddress searchAddress, AbstractDistanceCalculator distanceCalculator) {
        super();
        this.searchAddress = searchAddress;
        this.distanceCalculator = distanceCalculator;
    }

    @Override
    public Comparator<Job> toComparator() {
        
        return new Comparator<Job>() {

            @Override
            public int compare(Job job1, Job job2) {
                
                Location jobLocation1 = job1.getLocation();
                double distanceFrom1 = getDistanceForSortingPurposes(jobLocation1, searchAddress);
                
                Location jobLocation2 = job2.getLocation();
                double distanceFrom2 = getDistanceForSortingPurposes(jobLocation2, searchAddress);
                
                return Double.compare(distanceFrom1, distanceFrom2);
            }

            private double getDistanceForSortingPurposes(Location jobLocation, JobSearchAddress searchAddress) {

                if(!properlyDefined(searchAddress) || !properlyDefined(jobLocation)) {
                    return Double.MAX_VALUE;
                }
                
                double distance = distanceCalculator.from(jobLocation.getLatitude(), jobLocation.getLongitude())
                                                    .to(searchAddress.getLatitude(), searchAddress.getLongitude())
                                                    .calculate();
                
                return distance;
            }
            
            private boolean properlyDefined(Location location) {
                
                return location != null && !NullUtils.isAnyNull(location.getLatitude(), location.getLongitude());
            }
            
            private boolean properlyDefined(JobSearchAddress searchAddress) {
                
                return searchAddress != null && !NullUtils.isAnyNull(searchAddress.getLatitude(), searchAddress.getLongitude());
            }
        };
    }
    
    

}
