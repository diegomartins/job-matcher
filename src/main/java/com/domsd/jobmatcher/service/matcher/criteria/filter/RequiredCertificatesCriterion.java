package com.domsd.jobmatcher.service.matcher.criteria.filter;

import java.util.Set;

import com.domsd.jobmatcher.model.Job;
import com.domsd.jobmatcher.model.Worker;

public class RequiredCertificatesCriterion implements JobMatchCriterion {

    @Override
    public boolean matched(Worker worker, Job job) {
       
       Set<String> workerCertificates = worker.getCertificates();
       Set<String> requiredCertificates = job.getRequiredCertificates();
       
       if(requiredCertificates == null) {
           return true;
       }
       
       if(workerCertificates == null) {
           return false;
       }
       
       boolean workerHasAllRequiredCertificates = workerCertificates.containsAll(requiredCertificates);
       
       return workerHasAllRequiredCertificates;
    }

}
