package com.domsd.jobmatcher.service.matcher.criteria.filter;

import com.domsd.jobmatcher.model.Job;
import com.domsd.jobmatcher.model.JobSearchAddress;
import com.domsd.jobmatcher.model.Location;
import com.domsd.jobmatcher.model.Worker;
import com.domsd.jobmatcher.service.geo.AbstractDistanceCalculator;
import com.domsd.jobmatcher.service.geo.DistanceCalculatorFactory;
import com.domsd.jobmatcher.util.NullUtils;

public class DistanceCriterion implements JobMatchCriterion {
    
    private AbstractDistanceCalculator distanceCalculator;
    
    
    public DistanceCriterion() {
        distanceCalculator = DistanceCalculatorFactory.getDefault();
    }
    
    public DistanceCriterion(AbstractDistanceCalculator distanceCalculator) {
        this.distanceCalculator = distanceCalculator;
    }
    
    
    @Override
    public boolean matched(Worker worker, Job job) {
          
        Location jobLocation = job.getLocation();
        JobSearchAddress jobSearchAddress = worker.getJobSearchAddress();
        
        if(maxDistanceNotSpecified(jobSearchAddress)) {
            return true;
        }
        
        if(jobLocationNotSpecified(jobLocation)) {
            return false;
        }
       
        Integer maxDistance = jobSearchAddress.getMaxJobDistance();
        double actualDistance = calculateDistance(jobLocation, jobSearchAddress);
        
        return isWithinRange(maxDistance, actualDistance);
    }
   
    private double calculateDistance(Location jobLocation, JobSearchAddress jobSearchAddress) {
        
        Double jobSearchLatitude = jobSearchAddress.getLatitude();
        Double jobSearchLongitude = jobSearchAddress.getLongitude();
        
        Double jobLatitude = jobLocation.getLatitude();
        Double jobLongitude = jobLocation.getLongitude();
        
        double distance = distanceCalculator.from(jobSearchLatitude, jobSearchLongitude)
                                            .to(jobLatitude, jobLongitude)
                                            .calculate();
        
        return distance;
    }

    private boolean isWithinRange(Integer maxDistance, double actualDistance) {
        
        long roundedDistance = Math.round(actualDistance);
        
        return roundedDistance <= maxDistance;
    }
    
    private boolean jobLocationNotSpecified(Location jobLocation) {
        
        boolean jobLocationNotSpecified = jobLocation == null || 
                                          NullUtils.isAnyNull(jobLocation.getLatitude(),
                                                              jobLocation.getLongitude());
        
        return jobLocationNotSpecified;
    }

    private boolean maxDistanceNotSpecified(JobSearchAddress jobSearchAddress) {
        
        boolean maxDistanceNotSpecified = jobSearchAddress == null || 
                                          NullUtils.isAnyNull(jobSearchAddress.getLatitude(), 
                                                              jobSearchAddress.getLongitude(),
                                                              jobSearchAddress.getMaxJobDistance());
        
        return maxDistanceNotSpecified;
    }

}
