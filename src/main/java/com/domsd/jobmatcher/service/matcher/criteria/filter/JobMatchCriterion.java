package com.domsd.jobmatcher.service.matcher.criteria.filter;

import com.domsd.jobmatcher.model.Job;
import com.domsd.jobmatcher.model.Worker;

public interface JobMatchCriterion extends FilterCriterion<Worker, Job> {

}
