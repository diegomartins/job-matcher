package com.domsd.jobmatcher.repository.impl;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.domsd.jobmatcher.model.Job;
import com.domsd.jobmatcher.repository.JobRepository;
import com.domsd.jobmatcher.rest.RestClient;
import com.domsd.jobmatcher.service.configuration.ConfigurationService;

@Service
public class JobRestRepository implements JobRepository {

    @Inject
    private ConfigurationService configurationService;
    
    @Inject
    private RestClient restClient;
    
    
    @Override
    public List<Job> retrieveAllJobs() {

        Job[] jobs = restClient.get(configurationService.allJobsRestAPI(), Job[].class);

        return Arrays.asList(jobs);
    }

}
