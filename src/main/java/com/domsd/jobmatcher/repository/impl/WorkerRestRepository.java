package com.domsd.jobmatcher.repository.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.domsd.jobmatcher.model.Worker;
import com.domsd.jobmatcher.repository.WorkerRepository;
import com.domsd.jobmatcher.rest.RestClient;
import com.domsd.jobmatcher.service.configuration.ConfigurationService;

@Service
public class WorkerRestRepository implements WorkerRepository {
        
    @Inject
    private RestClient restClient;
    
    @Inject
    private ConfigurationService configurationService;
    
    @Override
    public List<Worker> retrieveAllWorkers() {
        
        Worker[] workers = restClient.get(configurationService.allWorkersRestAPI(), Worker[].class);
        
        if(workers == null) {
            return Collections.emptyList();
        }
        
        return Arrays.asList(workers);
    }

    //TODO: Find a way to request only one worker from the API
    @Override
    public Worker retrieveWorker(Long workerId) {
        
        List<Worker> allWorkers = retrieveAllWorkers();
        Optional<Worker> worker = allWorkers.stream().filter((each) -> workerId.equals(each.getId())).findFirst();
        
        return worker.orElse(null);
    }

}
