package com.domsd.jobmatcher.repository;

import java.util.List;

import com.domsd.jobmatcher.model.Job;

public interface JobRepository {

    List<Job> retrieveAllJobs();
}
