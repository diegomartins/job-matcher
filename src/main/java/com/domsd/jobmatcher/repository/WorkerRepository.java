package com.domsd.jobmatcher.repository;

import java.util.List;

import com.domsd.jobmatcher.model.Worker;

public interface WorkerRepository {

    List<Worker> retrieveAllWorkers();
    
    Worker retrieveWorker(Long workerId);
}
