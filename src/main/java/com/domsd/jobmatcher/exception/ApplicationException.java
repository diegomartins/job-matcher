package com.domsd.jobmatcher.exception;

public class ApplicationException extends RuntimeException {

	private static final long serialVersionUID = -7920590091572640165L;

	public ApplicationException(ErrorMessage errorMessage) {
		super(errorMessage.getMessage());
	}
}
