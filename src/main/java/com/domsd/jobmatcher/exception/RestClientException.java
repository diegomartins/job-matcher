package com.domsd.jobmatcher.exception;

import org.springframework.http.HttpStatus;

public class RestClientException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public RestClientException(Exception cause) {
        super(cause);
    }

    private HttpStatus status;
    private String message;
    private String debugMessage;
    
    public HttpStatus getStatus() {
        return status;
    }
    
    public void setStatus(HttpStatus status) {
        this.status = status;
    }
    
    public String getMessage() {
        return message;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }
    
    public String getDebugMessage() {
        return debugMessage;
    }
    
    public void setDebugMessage(String debugMessage) {
        this.debugMessage = debugMessage;
    }
    
    
    
}
