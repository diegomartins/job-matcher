package com.domsd.jobmatcher.exception;

public enum ErrorMessage {
	
    UNEXPECTED_ERROR("Unexpected error."),
	UNEXPECTED_NULL_IDENTIFIER("The worker identifier cannot be null."),
	
	ERROR_EXECUTING_INTERNAL_REQUEST("There was an error while executing an internal request."),
	
    ERROR_LOADING_WORKERS_DATA("Error loading workers' data."),
    ERROR_LOADING_JOBS_DATA("Error loading jobs' data."),
    
    WORKER_NOT_FOUND("Could not find the worker.");
    
	private String message;
	
	private ErrorMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
	
		
}
