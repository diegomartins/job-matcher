package com.domsd.jobmatcher.util;

import com.domsd.jobmatcher.exception.ApplicationException;
import com.domsd.jobmatcher.exception.ErrorMessage;

public class ValidatorUtils {

    public static void rejectIfNull(Object object, ErrorMessage errorMessage) {
        
        if(object == null) {
            throw new ApplicationException(errorMessage);
        }
    }
    
    public static void rejectIfAnyIsNull(ErrorMessage errorMessage, Object...objects) {
        
        for (Object object : objects) {
            rejectIfNull(object, errorMessage);
        }
    }
}
