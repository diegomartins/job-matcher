package com.domsd.jobmatcher.util;

public class NullUtils {

    public static boolean isAnyNull(Object... objects) 
    {
        for (Object object : objects) {
            
            if(object == null) {
                return true;
            }
        }
        
        return false;
    }
}
