package com.domsd.jobmatcher.util;

import java.math.BigDecimal;

import org.apache.commons.lang3.StringUtils;

public class CurrencyConverter {

    public static BigDecimal toBigDecimal(String currency) {

        if(StringUtils.isEmpty(currency)) {
            throw new RuntimeException("Failed to convert: Value cannot be empty.");
        }
        
        String valueWithoutSymbol = currency.replaceFirst("\\$", "");
        BigDecimal parsedValue = new BigDecimal(valueWithoutSymbol);

        return parsedValue;

    }
}
