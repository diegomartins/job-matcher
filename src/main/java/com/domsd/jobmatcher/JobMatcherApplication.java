package com.domsd.jobmatcher;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;

/*
 * TODO: Extract the configurations to use Spring Cloud/Consul,
 * so we will be able to change them without downtime
 * 
 * TODO: API Versioning
 * 
 * TODO: Use Resource Bundle
 */
@SpringBootApplication
public class JobMatcherApplication {

    @Inject
    private Environment env;

    private static Logger log = LoggerFactory.getLogger(JobMatcherApplication.class);
    
    //TODO: Move to a different class
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }
    
    @PostConstruct
    public void initApplication() {
        log.info("Running with Spring profile(s) : {}", Arrays.toString(env.getActiveProfiles()));
    }

    public static void main(String[] args) throws UnknownHostException {

        SpringApplication app = new SpringApplication(JobMatcherApplication.class);

        Environment env = app.run(args).getEnvironment();

        log.info(
                "\n----------------------------------------------------------\n\t"
                        + "Application '{}' is running! Access URLs:\n\t" 
                        + "Local: \t\thttp://127.0.0.1:{}\n\t"
                        + "External: \thttp://{}:{}\n"
                + "----------------------------------------------------------",
                env.getProperty("spring.application.name"), env.getProperty("server.port"),
                InetAddress.getLocalHost().getHostAddress(), env.getProperty("server.port"));

    }
}
