package com.domsd.jobmatcher.rest;

public interface RestClient {

    <T> T get(String url, Class<T> clazz);

    <T> T get(String url, Class<T> clazz, String errorMessage);

}