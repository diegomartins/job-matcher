package com.domsd.jobmatcher.rest;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.domsd.jobmatcher.exception.ErrorMessage;
import com.domsd.jobmatcher.exception.RestClientException;

/*
 * TODO: Extend this class to cover other HttpMethods
 * 
 * TODO: Make the try-catch more elegant (Interceptor/Callable<T>)
 * 
 * TODO: Better debug message, with request URL and HttpMethod
 */
@Service
public class RestClientImpl implements RestClient {

    @Inject
    private RestTemplate restTemplate;

    
    @Override
    public <T> T get(String url, Class<T> clazz) {

        return get(url, clazz, ErrorMessage.ERROR_EXECUTING_INTERNAL_REQUEST.getMessage());
    }

    @Override
    public <T> T get(String url, Class<T> clazz, String errorMessage) {

        try {

            T object = restTemplate.getForObject(url, clazz);

            return object;

        } catch (HttpClientErrorException e) {
            
            throw asRestClientException(e, errorMessage);
        }
    }
    
    private RestClientException asRestClientException(HttpClientErrorException e, String errorMessage) {

        RestClientException restClientException = new RestClientException(e);

        restClientException.setMessage(errorMessage);
        restClientException.setStatus(e.getStatusCode());
        restClientException.setDebugMessage(e.getMessage());

        return restClientException;
    }
}
