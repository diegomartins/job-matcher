package com.domsd.jobmatcher.web.rest;

import java.util.List;

import javax.inject.Inject;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.domsd.jobmatcher.model.Job;
import com.domsd.jobmatcher.service.matcher.JobMatcherService;

@RestController
@RequestMapping("/api")
public class JobMatchResource {
	
    @Inject
    private JobMatcherService jobMatcherService;
    
    //TODO: Transaction read only/not supported
	@GetMapping(value = "/matches/{workerId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Job>> matches(@PathVariable Long workerId) {
		
	    List<Job> matches = jobMatcherService.matches(workerId);
	    
		return ResponseEntity.ok().body(matches);
	}
	
}
